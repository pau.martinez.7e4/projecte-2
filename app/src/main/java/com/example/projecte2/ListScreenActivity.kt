package com.example.projecte2

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class ListScreenActivity : AppCompatActivity() {
    lateinit var title: TextView
    lateinit var rangoBajo: Button
    lateinit var rangoAlto: Button
    lateinit var rangoMaestro: Button
    lateinit var cuero: TextView
    lateinit var malla: TextView
    lateinit var cazador: TextView
    lateinit var hueso: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_screen)

        title = findViewById(R.id.title)
        rangoBajo = findViewById(R.id.buttonRB)
        rangoAlto = findViewById(R.id.buttonRA)
        rangoMaestro = findViewById(R.id.buttonRM)
        cuero = findViewById(R.id.cuero)
        malla = findViewById(R.id.malla)
        cazador = findViewById(R.id.cazador)
        hueso = findViewById(R.id.hueso)

        title.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        rangoBajo.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        rangoAlto.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        rangoMaestro.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        cuero.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        malla.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        cazador.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        hueso.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")

        cuero.setOnClickListener {
            val intent = Intent(this, DetailScreenActivity::class.java)
            startActivity(intent)
        }
    }
}