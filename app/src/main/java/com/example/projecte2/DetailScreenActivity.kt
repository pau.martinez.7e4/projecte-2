package com.example.projecte2

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class DetailScreenActivity : AppCompatActivity() {
    lateinit var armorName: TextView
    lateinit var material: TextView
    lateinit var diners: TextView
    lateinit var stats: TextView
    lateinit var skills: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_screen)

        //assignacio de fonts a la plantilla
        armorName = findViewById(R.id.nom_armadura)
        material = findViewById(R.id.materials_necessaris)
        diners = findViewById(R.id.diners_necessaris)
        stats = findViewById(R.id.stats)
        skills = findViewById(R.id.habilitats_propies)

        armorName.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        material.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        diners.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        stats.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        skills.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
    }
}