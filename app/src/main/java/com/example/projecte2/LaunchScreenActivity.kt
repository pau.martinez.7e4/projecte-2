package com.example.projecte2

import android.graphics.Typeface
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity



class LaunchScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        lateinit var textView1: TextView
        lateinit var textView2: TextView

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch_screen)

        textView1 = findViewById(R.id.textView)
        textView2 = findViewById(R.id.textView2)

        textView1.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
        textView2.typeface = Typeface.createFromAsset(assets, "fonts/monsterhunter.ttf")
    }
}